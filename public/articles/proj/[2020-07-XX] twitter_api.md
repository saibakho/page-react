# Twitter初探 -- Tweepy的簡易使用

## 前言
- 前陣子寫casino_poker後有點走火入魔，開始用selenium做更多壞壞的事，像是農月末戰貨之類的。寫著寫著就把腦筋動到方陣2.0，


```python
from tweepy import Stream, OAuthHandler
from tweepy.streaming import StreamListener

class Listener(StreamListener):
	def on_data(self, data):
		# (data is in JSON format)
		# do something when a tweet is tweeted and streamed
		return True

	def on_error(self, status):
		print(status)

def stream_twitter():
	CONSUMER_KEY = "AAAAAAAAAA"			# get the credential keys from
	CONSUMER_SECRET = "BBBBBBBBBB"		# your twitter developer account
	ACCESS_TOKEN = "CCCCCCCCCC"
	ACCESS_TOKEN_SECRET = "DDDDDDDDDD"

	auth = OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
	auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)
	streamer = Stream(auth, Listener())
	try:
		streamer.filter(track=["AAA BBB", "CCC DDD"])	# AAA and BBB or CCC and DDD for keyword
	finally:
		logging("streamer closed.")
```

- 從外部停止`streamer`

```python
	streamer.disconnect()
```