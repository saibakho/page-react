# Sāi-bak-hō's WebPage
- A webpage to record my own works and life.
- Try to deploy a static webpage with GitLab Page.

## to-do's
- Data Science section:
	- FP Growth
	- PCA
	- LSH
	- DGIM
	- or some more randomized algorithms
- MHWSetBuilder:
	- Expo Snack embedded in the webpage would be great!
	- But probably not working on this project anymore :(
- casino_poker for GBF:
	- Still can't solve the tapping problem (which is a core utility...)
	- Reference to jQuery Finger

## About the template setup for React with Webpack
- In the folder `#template_pack`.
- All stuffs are already set up, including `webpack.config.js`, `package.json`, and `.gitlab-ci.yml`.
- Just follow the steps to run a webpage instantly without dealing with those annoying dependencies. :D

### Before the developments:
```bash
	- npm install 		# to install all modules required
```

### Scripts:
```bash
	- npm run build		# to build and make bundle with webpack
	- npm run watch		# to watch bundling files with webpack
	- npm run start		# to start webpack-dev-server to launch website on local port
```