const path = require("path");
const srcPath = path.resolve(__dirname, "src");
const distPath = path.resolve(__dirname, "public");

module.exports = {
	entry: "./index.jsx",
	context: srcPath,
	resolve: {
		alias: {
			components: path.resolve(srcPath, "components")
		}
	},
	output: {
		filename: "[name].bundle.js",
		path: distPath,
	},
	module: {
		rules: [
			{
				test: /\.(js|jsx)$/,
				exclude: /node_modules/,
				use: {
					loader: "babel-loader",
					options: {
						presets: ["@babel/preset-react", "@babel/preset-env"]
					}
				}
			}, {
				test: /\.css$/,
				use: [
					"style-loader",
					{
						loader: "css-loader",
						options : {
							url: false
						}
					}
				]
			}
		]
	},
	devServer: {
		contentBase: distPath,
		port: 1234
	}
}