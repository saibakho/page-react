const path = require("path");
const srcPath = path.resolve(__dirname, "src");
const distPath = path.resolve(__dirname, "public");

module.exports = {
	entry: "./index.jsx",
	context: srcPath,
	resolve: {
		alias: {
			assets: path.resolve(srcPath, "assets"),
			screens: path.resolve(srcPath, "screens/Screens"),
			articles: path.resolve(distPath, "articles"),
			components: path.resolve(srcPath, "components/Components")
		},
		extensions: [".webpack.js", ".web.js", ".js", ".json", ".jsx"]
	},
	output: {
		path: distPath,
		filename: "[name].bundle.js"
	},
	module: {
		rules: [
			{
				test: /\.(js|jsx)$/,
				exclude: /node_modules/,
				use: {
					loader: "babel-loader",
					options: {
						presets: ["@babel/preset-react", "@babel/preset-env"]
					}
				}
			}, {
				test: /\.css$/,
				use: [
					"style-loader",
					{
						loader: "css-loader",
						options : {
							url: false
						}
					}
				]
			}, {
				test: /\.svg$/,
				loader: "@svgr/webpack"
			}, {
				test: /\.(jpg|png)$/,
				loader: "url-loader"
			}

		]
	},
	devServer: {
		//historyApiFallback: true,
		contentBase: distPath,
		port: 1234
	}
}