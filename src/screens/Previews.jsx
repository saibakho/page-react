import React, { useState } from "react"
import ReactMarkdown from "react-markdown/with-html"
import Markdown from 'markdown-to-jsx'
import { Route, Link } from "react-router-dom"
import { useSpring, animated, config } from "react-spring"

import { TextContainer } from "components"
import articles from "articles/articles.json"
import "./markdown.css"

export const Previews = ({header}) =>
<div style={styles.root}>
	<div style={styles.itemColumn}>
		<div style={styles.header}><TextContainer content={{
			"algo": "Algorithms",
			"proj": "My Projects",
			"about": "About Me"
		}[header]}/></div>
		<Route exact path={"/"+header+"/"} render={() => <ItemList header={header}/>}/>
		{articles[header].map((info) => <Route key={info.path} render={() => <Article header={header} info={info}/>}
			exact path={"/"+header+"/"+info.path.split(".")[0]}/>)}
		<div style={styles.pageNav}><TextContainer content="pages" color="gray"/></div>
	</div>
</div>

const ItemList = ({header}) =>
<div>{articles[header].map((info) => {
	let [hover, setHover] = useState(false);
	let readButton = useSpring({
		padding: "0.2rem 0.5rem",
		borderRadius: "0.2rem",
		backgroundColor: hover ? "#d3d3d3" : "white"
	})
	return (
		<div key={info.path} style={styles.items}>
			<div style={styles.marginRow}>
				<TextContainer align="flex-start" content={info.title} color="gray"/>
			</div>
			<div style={styles.line}/>
			<div style={styles.abstract}>
				<div style={styles.thumbsnail}>
					{info.image === "empty" ? <TextContainer content="No Image"/> :
					<img style={{width: "100%"}} src={"articles/"+header+"/img/"+info.image}/>}
				</div>
				<TextContainer content={info.abstract} size="1rem" color="gray"/>
			</div>
			<div style={styles.line}/>
			<div style={styles.marginRow}>
				<div><TextContainer content={info.date} size="1rem" color="gray"/></div>
				<div style={{flex: 1}}/>
				<animated.div style={readButton} onMouseEnter={() => setHover(true)} onMouseLeave={() => setHover(false)}>
					<Link to={"/"+header+"/"+info.path.split(".")[0]} style={{textDecoration: "none"}}>
						<TextContainer content=">>> 繼續閱讀" size="1rem" color={hover ? "white" : "gray"}/>
					</Link>
				</animated.div>
			</div>
		</div>
	)})
}</div>

const Article = ({header, info}) => {
	let [markdown, setMD] = useState("")
	// read file from distPath(/public)
	fetch("articles/"+header+"/"+info.path)
	.then((response) => response.text())
	.then((md) => setMD(md))
	return (
		<div style={styles.article}>
			<Markdown children={markdown}/>
		</div>
	)
}


const styles = {
	root: {
		display: "flex",
		justifyContent: "center"
	},
	itemColumn: {
		width: "70%",
		display: "flex",
		flexDirection: "column",
		justifyContent: "center",
	},
	article: {
		margin: "0.5rem",
		padding: "2rem 3rem",
		boxShadow: "0 0 0.3rem gray",
		borderRadius: "0.3rem",
		backgroundColor: "white",

		display: "flex",
		flexDirection: "column",
		justifyContent: "between",
	},
	items: {
		height: "20rem",
		margin: "0.5rem",
		boxShadow: "0 0 0.3rem gray",
		borderRadius: "0.3rem",
		backgroundColor: "white",

		display: "flex",
		flexDirection: "column",
	},
	line: {
		height: "0.1rem",
		margin: "0rem 1rem",
		backgroundColor: "lightGray",
	},
	marginRow: {
		display: "flex",
		flexDirection: "row",
		justifyContent: "between",
		margin: "0.3rem 1rem",
	},
	thumbsnail: {
		width: "15rem",
		height: "100%",
		borderRadius: "0.2rem",
		backgroundColor: "lightGray",
	},
	abstract: {
		flex: 1,
		display: "flex",
		alignItems: "center",
		flexDirection: "row",
		margin: "0.5rem 1rem",
	},
	header: {
		height: "10rem",
		margin: "0 0.5rem",
		display: "flex",
		flexDirection: "column",
		borderRadius: "0 0 0.3rem 0.3rem",
		backgroundColor: "#8cd98c",
		boxShadow: "0 0 0.3rem gray",
	},
	pageNav: {
		height: "3rem",
		margin: "0 0.5rem 2rem 0.5rem",
		display: "flex",
		flexDirection: "column",
		boxShadow: "0 0 0.3rem gray",
		borderRadius: "0.3rem",
		backgroundColor: "white",
	}
}