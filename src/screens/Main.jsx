import React from "react"
import {
	Route,
	HashRouter as Router
	//BrowserRouter as Router
} from "react-router-dom"
import { Home, Previews } from "screens"
import { Sidebar } from "components"


import Miu from "assets/img/asako_miu_bg.jpg"
/*import Ayako from "assets/img/asako_ayako_bg.jpg"
import Shiragiku from "assets/img/asako_shiragiku_bg.jpg"

import Camera from "assets/img/camera_bg.jpg"
import Emperor from "assets/img/black_emperor_bg.jpg"*/

export const Main = () => {
	return (
		<Router>
			<div style={styles.root}>
				<img src={Miu} style={styles.bg}/>
				<Sidebar/>
				<Route exact path="/" render={() => <Home/>}/>
				<Route path="/about" render={() => <Previews header="about"/>}/>
				<Route path="/algo" render={() => <Previews header="algo"/>}/>
				<Route path="/proj" render={() => <Previews header="proj"/>}/>
			</div>
		</Router>
	)
}

const styles = {
	root: {
		height: "100%",
	},
	bg: {
		width: "100%",
		position: "fixed",
		opacity: 0.8,
		zIndex: -1,
	}
}