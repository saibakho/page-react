import React from "react"
import { Link } from "react-router-dom"
import { animations } from "react-animation"

import { Previews } from "screens"
import { Button, TextContainer } from "components"

export const Home = () => {
	return (
		<div style={styles.root}>
			<div style={styles.mainPanel}>
				<div className="d-flex flex-column" style={{flex: 3}}>
					<Button style={{flex: 3}} content="My Projects" to="/proj"/>
					<div className="d-flex flex-row" style={{flex: 1}}>
						<div style={{flex: 2, ...styles.buttons}}/>
						<div style={{flex: 2, ...styles.buttons}}/>
						<Button style={{flex: 3}} content="test"/>
					</div>
				</div>
				<div className="d-flex flex-column" style={{flex: 2}}>
					<div className="d-flex flex-row" style={{flex: 3}}>
						<div className="d-flex flex-column" style={{flex: 3}}>
							<div style={{flex: 2, ...styles.buttons}}/>
							<Button style={{flex: 3}} content="About Me" to="/about"/>
						</div>
						<div style={{flex: 2, ...styles.buttons}}/>
					</div>
					<Button style={{flex: 2}} content="Algorithms" to="/algo"/>
				</div>
			</div>
		</div>
	)
}

const styles = {
	root: {
		height: "100%",
		display: "flex",
		alignItems: "center",
		justifyContent: "center"
	},
	mainPanel: {
		width: "1075px",//"70%",
		height: "650px",//"90%",
		display: "flex",
		flexDirection: "row"
	},
	buttons: {
		display: "flex",
		margin: "0.5rem",
		borderRadius: "0.5rem",
		backgroundColor: "#8cd98c",
		boxShadow: "0 0 0.5rem gray",
		textDecoration: "none",
		// react-animation
		animation: animations.popIn
	},
}
