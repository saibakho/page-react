import React from "react"
import ReactDOM from "react-dom"

import { Main } from "screens"

import "./index.css"
import "bootstrap/dist/css/bootstrap.css";

ReactDOM.render(
	<Main/>,
    document.getElementById("root")
)