import React, { useState } from "react"
import { Link } from "react-router-dom"
import { useSpring, animated, config } from "react-spring"

import { Home, Previews } from "screens"
import { Icon, NavButton, TextContainer } from "components"

import Cup from "assets/img/jon_burgerman_cup.png"
import Ayako from "assets/img/asako_ayako.jpg"

export const Sidebar = () => {
	let [hoverLeft , setHoverLeft ] = useState(false);
	let [hoverRight, setHoverRight] = useState(false);
	let slideInLeft = useSpring({
		config: config.slow,
		left: hoverLeft ? "0%" : "-100%",
		borderRadius: "0 0.3rem 0.3rem 0"
	}), slideInRight = useSpring({
		config: config.slow,
		right: hoverRight ? "0%" : "-100%",
		borderRadius: "0.3rem 0 0 0.3rem"
	})
	return (
		<div>
			<div style={styles.hover} onClick={() => {return false;}}
				onMouseEnter={() => setHoverLeft(true)}
				onMouseLeave={() => setHoverLeft(false)}>
				<animated.div style={{...styles.navbar, ...slideInLeft}}>

					<img src={Cup} style={styles.avatar}/>
					<NavButton to="/proj" style={styles.rowItem}/>
					<div style={styles.line}/>
					<NavButton to="/algo" style={styles.rowItem}/>
					<div style={styles.line}/>
					<NavButton to="/about" style={styles.rowItem}/>
					<div style={styles.line}/>

					<div style={{...styles.rowItem, height: "18rem"}}/>
					<div style={styles.widgets}>
						<Icon name="search" style={styles.icon}/>
						<Icon name="home" style={styles.icon}/>
						<Icon name="switch" style={styles.icon}/>
						<Icon name="palette" style={styles.icon}/>
						<Icon name="gear" style={styles.icon}/>
					</div>
					<TextContainer size="0.5rem" style={{...styles.rowItem, height: "1rem"}}
						content="Copyright©2020 Sāi-bak-hō"/>
				</animated.div>
			</div>
			
			<div style={{...styles.hover, right: 0}}
				onMouseEnter={() => setHoverRight(true)}
				onMouseLeave={() => setHoverRight(false)}>
				<animated.div style={{...styles.navbar, ...slideInRight}}></animated.div>
			</div>
		</div>
	)
}

const styles = {
	hover: {
		zIndex: 1,
		width: "15%",
		height: "100%",
		position: "fixed",
	},
	navbar: {
		height: "100%",
		padding: "0.5rem",
		position: "relative",

		display: "flex",
		alignItems: "center",
		flexDirection: "column",
		backgroundColor: "#5b8b5b",
	}, 
	line: {
		width: "90%",
		height: "0.1rem",
		backgroundColor: "white"
	},
	avatar: {
		width: "10rem",
		height: "10rem",
		margin: "0.5rem",
		borderRadius: "0.5rem",
		backgroundColor: "white",
	},
	rowItem: {
		width: "90%",
		height: "2rem",
		margin: "0.5rem",
		borderRadius: "0.2rem",
		//boxShadow: hover ? "" : "0 0 0.5rem white inset",
		//backgroundColor: "red",
	},
	widgets: {
		display: "flex",
		flexDirection: "row",

		//height: "5%",
		/*bottom: "1rem",
		position: "absolute",*/
	},
	icon: {
		width: "2.5rem",
	}
}