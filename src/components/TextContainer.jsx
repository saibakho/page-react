import React from "react"

export const TextContainer = ({
	style,
	font="Consolas",
	size="1.5rem",
	color="white",
	align="center",
	content="text",
}) => {
	return (
		<div style={{
			width: "100%",
			height: "100%",
			display: "flex",
			alignItems: "center",
			justifyContent: align,	// "flex-start", "flex-end", "center", "stretch", "baseline"
			...style,
		}}>
			<div style={{
				color: color,
				fontSize: size,
				fontFamily: font,
			}}>{content}</div>
		</div>
	)
}