import React, { useState } from "react"
import { Link } from "react-router-dom"
import { useSpring, animated, config } from "react-spring"

import { TextContainer } from "components"
import { Book, Edit, User } from "assets/svg/SVGs"

export const NavButton = ({style, to}) => {
	let [hover, setHover] = useState(false)
	let animation = useSpring({
		display: "flex",
		flexDirection: "row",
		backgroundColor: hover ? "white" : "#5b8b5b",
	})
	let styles = {
		icon: {
			height: "2rem",
			padding: "0.2rem 0.5rem",
			// svg attributes
			fill: hover ? "#5b8b5b" : "white",
		},
		link: {
			flex: 1,
			display: "flex",
			textDecoration: "none"
		}
	}
	return (
		<animated.div style={{...style, ...animation}}
			onMouseEnter={() => setHover(true)}
			onMouseLeave={() => setHover(false)}>
			{{	"/proj": <Edit style={styles.icon}/>,
				"/algo": <Book style={styles.icon}/>,
				"/about": <User style={styles.icon}/>}[to]}
			<Link to={to} style={styles.link}>
				<TextContainer align="flex-start" content={{
					"/proj": "My Projects",
					"/algo": "Algorithms",
					"/about": "About Me"
				}[to]} color={hover ? "#5b8b5b" : "white"}/>
			</Link>
		</animated.div>
	)
}