import React, { useState } from "react"
import { Link } from "react-router-dom"
import { useSpring, animated, config } from "react-spring"

import { TextContainer } from "components"

export const Button = ({to, style, color, content}) => {
	let [down, setDown] = useState(false)
	let [hover, setHover] = useState(false)
	let animation = useSpring({
		/*from: {
			width: "50%",
			height: "50%",
		},*/
		display: "flex",
		alignItems: "center",
		justifyContent: "center",
		width: "100%",
		height: "100%",
		padding: hover ? down ? "0.5rem" : "0rem" : "0.5rem",
	})
	let button = (
		<animated.div style={animation}
			onMouseUp={() => setDown(false)}
			onMouseDown={() => setDown(true)}
			onMouseEnter={() => setHover(true)}
			onMouseLeave={() => setHover(false)}>
			<TextContainer content={content} style={styles.button} size="2.5rem"/>
		</animated.div>
	)
	return to === undefined ?
		<div style={{...styles.root, ...style}}>{button}</div> :
		<Link style={{...styles.root, ...style}} to={to}>{button}</Link>
}

const styles = {
	root: {
		display: "flex",
		alignItems: "center",
		justifyContent: "center",
		textDecoration: "none",
	},
	button: {
		width: "100%",
		height: "100%",
		borderRadius: "0.5rem",
		backgroundColor: "#8cd98c",
		boxShadow: "0 0 0.5rem gray",
	}
}