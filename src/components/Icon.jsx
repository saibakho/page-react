import React, { useState } from "react"
import { Link } from "react-router-dom"
import { useSpring, animated, config } from "react-spring"

import {
	Gear,
	Close,
	House, 
	Search,
	Refresh,
	Palette,
	Settings,
} from "assets/svg/SVGs"

export const Icon = ({
	style,
	name="none"
}) => {
	let [hover, setHover] = useState(false)
	let icon = {
		width: "100%",
		height: "100%",
		fill: "#5b8b5b",
	}
	let inner = {
		cursor: "pointer",
		padding: "0.2rem",
		borderRadius: "0.2rem",
		backgroundColor: "white",
	}
	let animation = useSpring({
		...style,
		padding: hover ? "0rem" : "0.3rem"
	})
	return (
		<animated.div style={animation}
			onMouseEnter={()=>setHover(true)}
			onMouseLeave={()=>setHover(false)}>
			<div style={inner}>{{
				"gear": <Gear style={icon}/>,
				"none": <Close style={icon}/>,
				"home": <Link to="/">
							<House style={icon}/>
						</Link>,
				"search": <Search style={icon}/>,
				"switch": <Refresh style={icon}/>,
				"palette": <Palette style={icon}/>,
				"settings": <Settings style={icon}/>,
			}[name]}</div>
		</animated.div>
	)
}