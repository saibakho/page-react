import Book from "assets/svg/book.svg"
import Edit from "assets/svg/edit.svg"
import Gear from "assets/svg/gear.svg"
import User from "assets/svg/user.svg"

import Close from "assets/svg/close.svg"
import House from "assets/svg/house.svg"
import Search from "assets/svg/search.svg"

import Refresh from "assets/svg/refresh.svg"
import Palette from "assets/svg/color-palette.svg"
import Settings from "assets/svg/settings.svg"

export {
	Book,
	Edit,
	Gear,
	User,

	Close,
	House,
	Search,

	Refresh,
	Palette,
	Settings,
}